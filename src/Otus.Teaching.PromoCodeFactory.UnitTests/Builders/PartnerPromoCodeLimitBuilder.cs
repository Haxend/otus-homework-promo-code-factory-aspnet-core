﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class PartnerPromoCodeLimitBuilder
    {
        private DateTime? _cancelDate;
        private DateTime _endDate;

        public PartnerPromoCodeLimitBuilder()
        {
            _cancelDate = null;
            _endDate = DateTime.Now.AddDays(10);
        }

        public PartnerPromoCodeLimitBuilder WithCancelDate(DateTime? cancelDate)
        {
            _cancelDate = cancelDate;
            return this;
        }

        public PartnerPromoCodeLimitBuilder WithEndDate(DateTime? endDate)
        {
            _endDate = endDate == null ? _endDate = DateTime.Now.AddDays(10) : _endDate = (DateTime)endDate;
            return this;
        }

        public PartnerPromoCodeLimit Build()
        {
            return new PartnerPromoCodeLimit
            {
                CancelDate = _cancelDate,
                EndDate = _endDate
            };
        }
    }
}
