﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System.Threading.Tasks;
using Xunit;
using System;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            (_partnersRepositoryMock, _partnersController) = InitializeTestDependencies();
        }

        private (Mock<IRepository<Partner>>, PartnersController) InitializeTestDependencies()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            var partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            var partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
            return (partnersRepositoryMock, partnersController);
        }

        /// <summary>
        /// 1) Возврат 404, если партнер не найден
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync((Partner)null);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest());

            // Assert
            result.Should().BeOfType<NotFoundResult>();
        }

        /// <summary>
        /// 2) Возврат 400 с тектом "Данный партнер не активен", если партнер заблокирован
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithIsActive(false)
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest());

            // Assert
            var badRequestResult = result.Should().BeOfType<BadRequestObjectResult>().Subject;
            badRequestResult.Value.Should().Be("Данный партнер не активен");
        }

        /// <summary>
        /// 3.1) Обнуление количества выданных промокодов при установке нового лимита для партнера
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenSettingNewLimit_NumberIssuedPromoCodesIsReset()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var previousLimit = new PartnerPromoCodeLimitBuilder()
                .WithCancelDate(null)
                .WithEndDate(DateTime.Now.AddDays(10))
                .Build();
            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithIsActive(true)
                .WithNumberIssuedPromoCodes(5)
                .WithPartnerLimits(new List<PartnerPromoCodeLimit> { previousLimit })
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest { Limit = 10 });

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// 3.2) Отмена обнуления количества выданных промокодов партнеру при истекшем лимите
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenPreviousLimitIsExpired_NumberIssuedPromoCodesIsNotReset()
        {
            // Для правильной работы теста в контроллере была добавлена проверка EndDate лимита партнера
            // Arrange
            var partnerId = Guid.NewGuid();
            var expiredLimit = new PartnerPromoCodeLimitBuilder()
                .WithEndDate(DateTime.Now.AddDays(-1))
                .Build();
            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithIsActive(true)
                .WithNumberIssuedPromoCodes(5)
                .WithPartnerLimits(new List<PartnerPromoCodeLimit> { expiredLimit })
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest { Limit = 10 });

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(5);
        }

        /// <summary>
        /// 4) Отключение предыдущего лимита при установке нового, пинг 1 секунда :)
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenSettingNewLimit_PreviousLimitIsCancelled()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var previousLimit = new PartnerPromoCodeLimitBuilder()
                .WithCancelDate(null)
                .WithEndDate(DateTime.Now.AddDays(10))
                .Build();
            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithIsActive(true)
                .WithPartnerLimits(new List<PartnerPromoCodeLimit> { previousLimit })
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest { Limit = 10 });

            // Assert
            previousLimit.CancelDate.Should().BeCloseTo(DateTime.Now, 1000);
        }

        /// <summary>
        /// 5) Лимит должен быть больше 0
        /// </summary>
        /// <returns></returns>
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task SetPartnerPromoCodeLimitAsync_WhenLimitIsLessThanOrEqualToZero_ReturnsBadRequest(int limit)
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithIsActive(true)
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest { Limit = limit });

            // Assert
            var badRequestResult = result.Should().BeOfType<BadRequestObjectResult>().Subject;
            badRequestResult.Value.Should().Be("Лимит должен быть больше 0");
        }

        /// <summary>
        /// 6) Убеждаемся в том, что новый лимит был сохранен в БД
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenSettingNewLimit_NewLimitIsSaved()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithIsActive(true)
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest { Limit = 10 });

            // Assert
            _partnersRepositoryMock.Verify(repo => repo.UpdateAsync(It.Is<Partner>(p => p.PartnerLimits.Any(l => l.Limit == 10))), Times.Once);
        }

        /// <summary>
        /// Проверка работы метода GetActiveLimit
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenMultipleLimitsExist_CorrectActiveLimitIsCanceled()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var activeLimit = new PartnerPromoCodeLimitBuilder()
                .WithCancelDate(null)
                .WithEndDate(DateTime.Now.AddDays(5))
                .Build();
            var canceledLimit = new PartnerPromoCodeLimitBuilder()
                .WithCancelDate(DateTime.Now.AddDays(-5))
                .WithEndDate(DateTime.Now.AddDays(10))
                .Build();
            var expiredLimit = new PartnerPromoCodeLimitBuilder()
                .WithCancelDate(null)
                .WithEndDate(DateTime.Now.AddDays(-1))
                .Build();
            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithIsActive(true)
                .WithPartnerLimits(new List<PartnerPromoCodeLimit> { activeLimit, canceledLimit, expiredLimit })
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest { Limit = 10 });

            // Assert
            activeLimit.CancelDate.Should().BeCloseTo(DateTime.Now, 1000);
        }
    }
}